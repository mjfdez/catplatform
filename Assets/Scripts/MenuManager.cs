﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{


    public void Finish()
    {
		GameObject.FindGameObjectsWithTag("Finish");
        Application.Quit();
    }

    public void Exit()
    {
        Debug.Log("EXIT");
        Application.Quit();
    }

    public void Play()
    {
        Debug.Log("START");
        SceneManager.LoadScene("Game");
    }
}
 