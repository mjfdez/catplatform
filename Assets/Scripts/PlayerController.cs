﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Variables publicas para poder modifircar desde el inspector
    public float horizontalVelocity = 11f;
    public float jumpForce = 500f;

    private Rigidbody2D rb2d = null;
    private float move = 0f;
    private Animator anim;
    private bool flipped = false;
    private bool slide = false;
    private bool dead = false;



    private Vector3 iniPos;

    // Use this for initialization
    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();

        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (dead)
        {
            return;
        }

        //Miramos el input Horizontal
        move = Input.GetAxis("Horizontal");
		slide = Input.GetKeyDown (KeyCode.DownArrow);
        rb2d.velocity = new Vector2(move * horizontalVelocity * Time.fixedDeltaTime, rb2d.velocity.y);

        if (Input.GetButtonDown("Jump") && (Mathf.Abs(rb2d.velocity.y) < 0.001f))
        {
            rb2d.AddForce(Vector2.up * jumpForce);
        }

        if (slide && (Mathf.Abs(rb2d.velocity.y) < 0.001f))
        {
            anim.SetTrigger ("slide");
        }
        //Miramos si nos estamos moviendo.
        // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
        if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
        {
            if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
        }

        anim.SetFloat("hvelocity", Mathf.Abs(rb2d.velocity.x));
        anim.SetFloat("vvelocity", rb2d.velocity.y);	
    }
		
    void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "muerte")
		{
			dead = true;
            anim.SetTrigger("dead");
        }
	}

    public void ResetMuerte()
    {
        Debug.LogError("Reseteo Muerte");
        transform.position = iniPos;
        dead = false;
        anim.Play("CatIdle");
        
    }


}

